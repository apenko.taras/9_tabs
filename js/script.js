tabsContent.addEventListener("click", function (event){
    let li = event.target
    const tabs = document.getElementsByClassName('active')
    tabs[0].classList.remove('active')
    li.classList.add('active')
    let tabContent = document.getElementsByClassName('tabs-content')[0].children
    for (let n = 0; n < tabContent.length; n++){
        tabContent[n].hidden = true
        if (tabContent[n].dataset.nameTab == li.textContent.toLowerCase()){
            tabContent[n].hidden = false
        }
    }
} )